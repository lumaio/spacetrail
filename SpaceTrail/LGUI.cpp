#include "LGUI.h"

LGUI::LGUI()
{
}

LGUI::~LGUI()
{
}

void LGUI::clearWidgets()
{
	m_widgets.clear();
}

void LGUI::removeWidget(std::shared_ptr<LWidget> w)
{
	for (int i = 0; i < m_widgets.size(); i++)
	{
		if (m_widgets[i] == w)
		{
			printf("found widget\n");
			m_widgets.erase(m_widgets.begin() + i);
		}
	}
}

void LGUI::addWidget(std::shared_ptr<LWidget> widget)
{
	m_widgets.emplace_back(widget);
}

void LGUI::handleEvents(thor::ActionMap<std::string>& emap)
{
	for each(auto widget in m_widgets)
	{
		if (widget)
			widget->handleEvents(emap);
		else
			printf("fucking widget is null or something\n");
	}
}

void LGUI::update()
{
	for each(auto widget in m_widgets)
	{
		if (widget)
			widget->update();
	}
}

void LGUI::render(sf::RenderWindow* context)
{
	for each(auto widget in m_widgets)
	{
		if (widget) {
			widget->render(context);

			if (widget->needsDestroy())
				removeWidget(widget);
		}
	}
}

void LGUI::disableAll()
{
	for each(auto widget in m_widgets)
		widget->setEnabled(false);
}
