#pragma once
#include "GameState.h"
#include "MenuState.h"
#include "PlayState.h"
#include "LGUI.h"
#include "LButton.h"
#include "LCheckBox.h"
#include "LLabel.h"
#include "LOutput.h"
#include "LNumberBox.h"
#include "Ship.h"
#include "Crew.h"

struct HireOption
{
	std::shared_ptr<Crew> crew;
	std::shared_ptr<LCheckBox> hire;
	std::shared_ptr<LLabel> dext;
	std::shared_ptr<LLabel> health;

	HireOption(float x, float y, float w)
	{
		crew = std::make_shared<Crew>();
		hire = std::make_shared<LCheckBox>(crew->name_first + " " + crew->name_last, x, y, false, 175);
		dext = std::make_shared<LLabel>("Dext: " + std::to_string(int(crew->dexterity)), x + 20, y + 25, A_LEFT, 16, 125);
		health = std::make_shared<LLabel>("Health: " + std::to_string(int(crew->health)), x + 20, y + 50, A_LEFT, 16, 125);
	}
};

class HireState :
	public GameState
{
private:
	std::vector<std::shared_ptr<HireOption>> hirecrew;

	std::shared_ptr<LOutput> output;

	std::shared_ptr<LNumberBox> food;
	std::shared_ptr<LNumberBox> fuel;
	std::shared_ptr<LLabel> credits;

	std::shared_ptr<LGUI> gui;

	int credits_left;

public:
	HireState()=default;
	explicit HireState(Engine* e);
	~HireState();

	void init(EventMap&) override;
	void leave() override;
	void update(float, sf::Event&) override;
	void render(sf::RenderWindow*) override;

};

extern GameState* hirestate;
