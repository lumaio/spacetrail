#pragma once

#include <iostream>
#include <vector>

struct Ship
{
	int food = 0; // 0
	int maxfood = 20; // 20
	int fuel = 0;
	int maxfuel = 10;
	int speed = 5; // 5

	int weight = 0; // dunno
	int maxweight = 10;

	int credits= 1500;

	Ship() {

	}

	int getFood() {
		return food;
	}
	int getFuel() {
		return fuel;
	}
	int getMaxFood() {
		return maxfood;
	}
	int getMaxFuel() {
		return maxfuel;
	}
	int getSpeed() {
		return speed;
	}
	int getWeight() {
		return weight;
	}
	int getMaxWeight() {
		return maxweight;
	}


	void setFood(int f) {
		food = f;
	}
	void setFuel(int f) {
		fuel = f;
	}
	void setWeight(int w) {
		weight = w;
	}

};

extern Ship* ship;
