#include "NameGenerator.h"

namespace NameGen
{

	const std::vector<std::string> NamePrefix = {
		"bel", "nar", "xan",
		"bell", "natr", "ev",
		"adur", "aes", "anim", "apoll", "imac",
		"educ", "equis", "extr", "guius", "hann",
		"equi", "amora", "hum", "iace", "ille",
		"inept", "iuv", "obe", "ocul", "orbis"
	};

	const std::vector<std::string> NameSuffix = {
		"us", "ix", "ox", "ith",
		"ath", "um", "ator", "or", "axia",
		"imus", "ais", "itur", "orex", "o"
		"y"
	};


	std::string NameGen::NameGen()
	{
		std::ostringstream ss;
		std::string PlayerName;

		ss << NamePrefix[(rand() % NamePrefix.size())];
		ss << NameSuffix[(rand() % NameSuffix.size())];

		PlayerName = ss.str();
		PlayerName[0] = std::toupper(PlayerName[0]);

		return PlayerName;
	}

}
