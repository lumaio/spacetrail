#pragma once
#include <vector>
#include "LGUI.h"
#include "LWidget.h"
#include "LButton.h"

class LEventBox :
	public LWidget
{
private:
	std::string title;
	std::string body;

	float globalWidth;

	bool usebg;
	bool loadNew;

	sf::Font font;
	sf::Text text_title;
	sf::Text text_body;

	sf::Texture bg_t;
	sf::Sprite bg_s;
	sf::Color bg_c;

	std::vector<std::shared_ptr<LButton>> options;

	std::shared_ptr<LGUI> gui;

	float w, h;

public:
	LEventBox(float, float, std::string, std::string, bool=false);
	LEventBox(float, float, std::string, std::string, sf::Color);
	~LEventBox();

	void addOption(std::string, std::function<void()>);

	void handleEvents(thor::ActionMap<std::string>&) override;
	void update() override;
	void render(sf::RenderWindow*) override;

	void setSize(float, float);
	void setBackground(bool);

	void destroy() override;

	//std::shared_ptr<LEventBox> setSize(double, double);

};

