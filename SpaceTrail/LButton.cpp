#include "LButton.h"


LButton::LButton(std::string t, float x, float y, bool aw, float w, int c, std::string f) :
	c1(16),
	aw(aw),
	scale(1),
	enabled(true),
	LWidget()
{
	this->x = x;
	this->y = y;
	this->_text = t;

	callback = [&]() {}; // default callback. does nothing.

	font.loadFromFile(f);
	text.setFont(font);
	text.setString(t);
	text.setCharacterSize(c);

	if (aw)
		this->w = text.getGlobalBounds().width*1.5 + 20;
	else
		this->w = w;

	((sf::Texture&)font.getTexture(16)).setSmooth(false);


}

LButton::~LButton()
{
}

bool LButton::inBounds(sf::Vector2i m)
{
	float s_l = text.getPosition().x;
	float s_r = text.getPosition().x + text.getGlobalBounds().width;
	float s_t = text.getPosition().y + 5;
	float s_b = text.getPosition().y + text.getCharacterSize() * scale;
	return (m.x > s_l && m.x < s_r && m.y > s_t && m.y < s_b);
}

void LButton::connect(std::function<void()> callback)
{
	this->callback = callback;
}

void LButton::handleEvents(thor::ActionMap<std::string>& emap)
{
	if (!enabled) return;

	if (emap.isActive("click_left")) if (inBounds(mpos)) callback();
	
	if (inBounds(mpos))
	{
		if (emap.isActive("hold_left") && inBounds(mpos))
			scale = 1.25;
		else
			scale = 1.5;
	}
	else
		scale = 1;

	// how about them nests tho
}

void LButton::update()
{

	text.setScale(scale, scale);

	float tw = text.getGlobalBounds().width;
	text.setPosition(floor(x + ((w / 2) - (tw / 2))), y - ceil(text.getCharacterSize() * scale / 1.3));


}

void LButton::render(sf::RenderWindow* context)
{
	mpos = sf::Mouse::getPosition(*context);

	float tw = text.getGlobalBounds().width;
	float z = (x + ((w / 2) - (tw / 2)));

	sf::Color c;

	if (enabled)
		c = sf::Color(0, 255, 255);
	else
		c = sf::Color(175, 175, 175);

	sf::Vertex left[2] = {
		sf::Vertex(sf::Vector2f(x, y), c),
		sf::Vertex(sf::Vector2f(z - 2, y), c)
	};
	sf::Vertex right[2] = {
		sf::Vertex(sf::Vector2f(z + tw + 4, y), c),
		sf::Vertex(sf::Vector2f(x + w, y), c)
	};

	context->draw(left, 2, sf::Lines);
	context->draw(right, 2, sf::Lines);
	context->draw(text);
}

void LButton::setEnabled(bool e)
{
	enabled = e;
}

bool LButton::isEnabled()
{
	return enabled;
}

float LButton::getWidth()
{
	return w;
}
