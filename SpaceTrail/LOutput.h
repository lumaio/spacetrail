#pragma once
#include <iostream>
#include <vector>
#include "LWidget.h"

enum SEVERITY
{
	S_ERROR,
	S_INFO,
	S_WARNING,
	S_NONE
};

struct Output
{
	std::string prefix;
	std::string text;
	SEVERITY severity;
};

class LOutput :
	public LWidget
{
private:
	std::vector<std::shared_ptr<Output>> output;

	sf::Font font;
	sf::Text text;

	sf::RectangleShape box;

	float w,h;
	bool drawbox;

public:
	LOutput() = default;
	LOutput(float, float, float, float, bool = false);
	~LOutput();

	void print(std::string, SEVERITY = S_NONE);

	void update() override;
	void render(sf::RenderWindow*) override;

};

