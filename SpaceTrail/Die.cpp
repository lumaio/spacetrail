#include "Die.h"

Die::Die(float x, float y) :
	timer(0),
	timer2(0),
	rolling(false)
{
	sides[0].loadFromFile("data/images/one.png");
	sides[1].loadFromFile("data/images/two.png");
	sides[2].loadFromFile("data/images/three.png");
	sides[3].loadFromFile("data/images/four.png");
	sides[4].loadFromFile("data/images/five.png");
	sides[5].loadFromFile("data/images/six.png");

	sprite.setTexture(sides[rand() % 6]);
	sprite.setPosition(x, y);
}

Die::~Die()
{
}

void Die::roll()
{
	if (!rolling)
		rolling = true;
}

bool Die::done() const
{
	return !rolling;
}

int Die::getNumber() const
{
	if (!rolling)
		return number;
}

const sf::Texture& Die::getTexture() const
{
	return *sprite.getTexture();
}

void Die::update(float dt)
{
	if (rolling)
	{
		timer += dt;
		timer2 += dt;
		if (timer > 4)
		{
			timer = 0;
			timer2 = 0;
			rolling = false;
			auto chosen = rand() % 6;
			sprite.setTexture(sides[chosen]);

			number = chosen + 1;
		}
		
		if (rolling && timer2 > 0.05)
		{
			sprite.setTexture(sides[rand() % 6]);
			timer2 = 0;
		}
	}
}

void Die::render(sf::RenderWindow* context) const
{
	context->draw(sprite);
}
