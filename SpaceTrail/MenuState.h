#pragma once
#include "GameState.h"
#include "HireState.h"
#include "CombatState.h"

#include "LGUI.h"
#include "LButton.h"
#include "LLabel.h"
#include "LCheckBox.h"
#include "LNumberBox.h"
#include "LEventBox.h"

#include "Die.h"

class MenuState :
	public GameState
{
private:
	/// Gui
	std::shared_ptr<LButton> start;
	std::shared_ptr<LButton> options;
	std::shared_ptr<LButton> quit;

	std::shared_ptr<LGUI> gui;

public:
	MenuState() = default;
	explicit MenuState(Engine*);
	~MenuState();

	void init(EventMap&) override;
	void leave() override;
	void update(float, sf::Event&) override;
	void render(sf::RenderWindow*) override;

};

extern GameState* menustate;

