#include "HireState.h"

HireState::HireState(Engine* e) :
	GameState(e)
{
}

HireState::~HireState()
{
}

void HireState::init(EventMap& emap)
{
	emap["pause"] = thor::Action(sf::Keyboard::Escape);

	gui = std::make_shared<LGUI>();

	output = std::make_shared<LOutput>(10, 10, 325, 200);

	gui->addWidget(output);

	srand(unsigned int(time(nullptr)));
	printf("init\n");

	/////////////////////////////
	//////// HIRE CREW //////////

	gui->addWidget(std::make_shared<LLabel>("Select crew to hire. Max 5", 0, 10, A_CENTER, 16, 1280));

	for (int i = 0; i < 5; i++)
	{
		std::shared_ptr<HireOption> ho = std::make_shared<HireOption>(1280/2-185, 50 + i * 78, 0);
		hirecrew.emplace_back(ho);
		gui->addWidget(ho->hire);
		gui->addWidget(ho->dext);
		gui->addWidget(ho->health);
	}
	for (int i = 0; i < 5; i++)
	{
		std::shared_ptr<HireOption> ho = std::make_shared<HireOption>(1280/2+10, 50 + i * 78, 0);
		hirecrew.emplace_back(ho);
		gui->addWidget(ho->hire);
		gui->addWidget(ho->dext);
		gui->addWidget(ho->health);
	}

	/////////////////////////////
	////// BUY RESOURCES ////////

	float newx = (1280 / 2);

	gui->addWidget(std::make_shared<LLabel>("Resources to buy.", 0, 450, A_CENTER, 16, 1280));
	credits = std::make_shared<LLabel>(std::to_string(ship->credits) + "c", 1280 / 2 - 50, 475, A_CENTER, 16, 100);
	gui->addWidget(std::make_shared<LLabel>("Food", 1280 / 2 - 100, 500, A_LEFT, 16, 75));
	gui->addWidget(std::make_shared<LLabel>("Fuel", 1280 / 2 + 25, 500, A_RIGHT, 16, 75));
	food = std::make_shared<LNumberBox>(newx - 100, 515, 75, 20, 0);
	fuel = std::make_shared<LNumberBox>(newx + 25, 515, 75, 20, 0);

	gui->addWidget(food);
	gui->addWidget(fuel);
	gui->addWidget(credits);

	/////////////////////////////
	////////// DONE ////////////

	std::shared_ptr<LButton> embark(new LButton("Embark", 1280.f / 2 - 100, 550.f, false, 200.f));
	embark->connect([&]() {
		int numchecked = 0;
		for (auto& c : hirecrew) if (c->hire->isChecked()) numchecked++;

		if (numchecked > 5 || numchecked < 5)
		{
			output->print("You must pick 5. No more, no less.", S_ERROR);
			return;
		}

		if (credits_left < 0)
		{
			output->print("Insufficiant credits.", S_ERROR);
			return;
		}

		if (food->getNumber() == 0)
			output->print("You won't have any food.", S_WARNING);
		if (fuel->getNumber() == 0)
			output->print("You won't have any fuel.", S_WARNING);

		output->print("Success! This is just a test for now.", S_INFO);

		ship->credits = credits_left;
		ship->food = food->getNumber();
		ship->fuel = fuel->getNumber();
		m_engine->setState(playstate);

		

	});

	gui->addWidget(embark);

}

void HireState::leave() { }

void HireState::update(float dt, sf::Event& event)
{
	if (m_engine->getEventMap().isActive("pause"))
		m_engine->setState(menustate);

	credits_left = (ship->credits - food->getNumber() * 100) - fuel->getNumber() * 100;
	credits->setText(std::to_string(credits_left) + "c");

	gui->handleEvents(m_engine->getEventMap());
	gui->update();

}

void HireState::render(sf::RenderWindow* context)
{

	sf::Vertex split[2] = {
		sf::Vertex(sf::Vector2f(0, 430)),
		sf::Vertex(sf::Vector2f(1280, 430))
	};
	sf::Vertex split2[2] = {
		sf::Vertex(sf::Vector2f(0, 525)),
		sf::Vertex(sf::Vector2f(1280, 525))
	};

	if (gui != nullptr)
		gui->render(context);

	//context->draw(split, 2, sf::Lines);
	//context->draw(split2, 2, sf::Lines);
}
