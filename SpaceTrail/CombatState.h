#pragma once
#include <algorithm>
#include "GameState.h"
#include "Die.h"
#include "Crew.h"
#include "LGUI.h"
#include "LEventBox.h"

struct vector2 {
	float x;
	float y;

	vector2(void) :
		x(0),
		y(0)
	{ }

	vector2(float x, float y) :
		x(x),
		y(y)
	{ }

	float length() const {
		return sqrtf(x * x + y * y);
	}

	vector2 normal() const {
		vector2 vector;
		auto len = length();
		if (len != 0)
		{
			vector.x = x / len;
			vector.y = y / len;
		}
		return vector;
	}

	vector2 operator-(const vector2& right) const {
		return vector2(this->x - right.x, this->y - right.y);
	}

	vector2 operator+(const vector2& right) const {
		return vector2(this->x + right.x, this->y + right.y);
	}
	vector2 operator+(const float& right) const {
		return vector2(this->x + right, this->y + right);
	}

	vector2 operator*(const vector2& right) const {
		return vector2(this->x * right.x, this->y * right.y);
	}

	vector2 operator*(const float& right) const {
		return vector2(this->x * right, this->y * right);
	}

	vector2 operator*(const int& right) const {
		return vector2(this->x * right, this->y * right);
	}

};

class CombatState :
	public GameState
{
private:

	sf::Shader glow;
	sf::RenderTexture buffer;

	std::shared_ptr<Die> player_die_;
	std::shared_ptr<Die> enemy_die_;

	// this is just temporary
	std::shared_ptr<Crew> player_crew_;
	std::shared_ptr<Crew> enemy_crew_;

	bool clicked;
	std::shared_ptr<LGUI> gui;

	sf::VertexArray verts;

	int num;
	float time;

	int player_stamina_;

public:
	CombatState() = default;
	explicit CombatState(Engine*);
	~CombatState();

	void init(EventMap&) override;
	void leave() override;
	void update(float, sf::Event&) override;
	void render(sf::RenderWindow*) override;

};

extern GameState* combatstate;
