#include "LEventBox.h"


LEventBox::LEventBox(float x, float y, std::string title, std::string body, bool usebg) :
	title(title),
	body(body),
	globalWidth(),
	loadNew(true),
	usebg(usebg),
	bg_c(sf::Color::Transparent),
	LWidget()
{

	gui = std::make_shared<LGUI>();

	this->x = x;
	this->y = y;

	font.loadFromFile("data/fonts/proggy.ttf");

	text_title.setFont(font);
	text_title.setString(title);
	text_title.setPosition(x + 5, y - 12);
	text_title.setCharacterSize(16);
	
	text_body.setFont(font);
	text_body.setString(body);
	text_body.setPosition(x + 10, y + 15);
	text_body.setCharacterSize(16);

	h = text_title.getGlobalBounds().height + 30 + text_body.getGlobalBounds().height + 20;

}

LEventBox::LEventBox(float x, float y, std::string title, std::string body, sf::Color color) :
	LEventBox(x, y, title, body, false)
{
	bg_c = color;
	printf("color\n");
}

LEventBox::~LEventBox()
{
}

void LEventBox::addOption(std::string text, std::function<void()> funct)
{
	loadNew = true;
	// add to the width
	auto button = std::make_shared<LButton>(text, x + 5 + globalWidth + 5, y + h - 10, true);
	button->connect(funct);

	options.push_back(button);
	gui->addWidget(button);

	globalWidth += 5;
	globalWidth += button->getWidth();

}

void LEventBox::handleEvents(thor::ActionMap<std::string>& emap)
{
	if (gui != nullptr)
		gui->handleEvents(emap);
}

void LEventBox::update()
{
	if (gui != nullptr)
		gui->update();

	bg_s.setPosition(x, y);

	if (loadNew)
	{
		int _x = 1920 - globalWidth;
		int _y = 1080 - h;
		bg_t.loadFromFile("data/images/background.png", sf::IntRect(_x - rand() % _x, _y - rand() % _y, globalWidth + 20, h));
		bg_s.setTexture(bg_t);
		loadNew = false;
	}

}

void LEventBox::render(sf::RenderWindow* context)
{
	
	sf::Vertex box[6] = {
		sf::Vertex(sf::Vector2f(x + 3, y), sf::Color::Green),
		sf::Vertex(sf::Vector2f(x, y), sf::Color::Green),
		sf::Vertex(sf::Vector2f(x, y + h), sf::Color::Green),
		sf::Vertex(sf::Vector2f(x + globalWidth + 20, y + h), sf::Color::Green),
		sf::Vertex(sf::Vector2f(x + globalWidth + 20, y), sf::Color::Green),
		sf::Vertex(sf::Vector2f(x + 5 + text_title.getGlobalBounds().width + 4, y), sf::Color::Green)
	};

	if (usebg)
		context->draw(bg_s);

	context->draw(box, 6, sf::LinesStrip);
	context->draw(text_title);
	context->draw(text_body);

	gui->render(context);
}

void LEventBox::setSize(float w, float h)
{
	this->globalWidth = w - 20;
	this->h = h;
}

void LEventBox::setBackground(bool use)
{
	usebg = use;
}

void LEventBox::destroy()
{
	for each(auto b in options)
		gui->removeWidget(b);

	LWidget::destroy();
}
