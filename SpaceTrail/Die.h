#pragma once
#include <SFML/Graphics.hpp>

class Die
{
private:
	float timer;
	float timer2;
	bool rolling;
	int number;

	sf::Texture sides[6];
	sf::Sprite sprite;

public:
	Die(float, float);
	~Die();

	void roll();
	bool done() const;
	int getNumber() const;
	const sf::Texture& getTexture() const;

	void update(float);
	void render(sf::RenderWindow*) const;
};

