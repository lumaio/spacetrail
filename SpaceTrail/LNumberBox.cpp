#include "LNumberBox.h"

LNumberBox::LNumberBox(float x, float y, int w, int max, int min, int n) :
	num(n),
	w(w),
	t_offset(0),
	b_offset(0),
	max(max),
	min(min),
	LWidget()
{
	this->x = x;
	this->y = y;

	font.loadFromFile("data/fonts/proggy.ttf");

	text.setFont(font);
	text.setCharacterSize(16);
	text.setString("0");

	highlight.setPosition(0, 0);
	highlight.setFillColor(sf::Color::Transparent);
	highlight.setSize(sf::Vector2f(10, 5));

}

LNumberBox::~LNumberBox()
{
}

LNumberBox::bounds LNumberBox::inBounds()
{
	float left = x + w;
	float right = left + 10;

	if (mpos.x > left && mpos.x < right && mpos.y > y - 5 + t_offset && mpos.y < y + t_offset)
		return B_UPPER;
	if (mpos.x > left && mpos.x < right && mpos.y > y + 14 - b_offset && mpos.y < y + 14 + 5 - b_offset)
		return B_LOWER;

	return B_NONE;

}

void LNumberBox::handleEvents(thor::ActionMap<std::string>& emap)
{

	switch (inBounds())
	{
	case B_UPPER:
		t_offset = 1;
		if (emap.isActive("click_left") && num < max)
			num++;
		break;
	case B_LOWER:
		b_offset = -1;
		if (emap.isActive("click_left") && num > min)
			num--;
		break;
	case B_NONE:
		t_offset = 0;
		b_offset = 0;
		break;
	}

	if (inBounds() == B_UPPER && emap.isActive("hold_left"))
		t_offset = 2;
	if (inBounds() == B_LOWER && emap.isActive("hold_left"))
		b_offset = -2;

}

void LNumberBox::update()
{
	text.setString(std::to_string(num));
	text.setPosition(floor((x + w / 2) - (text.getLocalBounds().width / 2)), floor(y - 5));

}

void LNumberBox::render(sf::RenderWindow* context)
{
	mpos = sf::Mouse::getPosition(*context);

	float z = ((x + w / 2) - (text.getLocalBounds().width / 2));

	sf::Vertex left[2] = {
		sf::Vertex(sf::Vector2f(x, y + 7), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(z - 2, y + 7), sf::Color(0, 255, 255))
	};
	sf::Vertex right[2] = {
		sf::Vertex(sf::Vector2f(z + text.getLocalBounds().width + 4, y + 7), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w, y + 7), sf::Color(0, 255, 255))
	};

	float y2 = y + 14;

	sf::Vertex top[3] = {
		sf::Vertex(sf::Vector2f(x + w,      y + t_offset), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w + 5,  y - 5 + t_offset), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w + 10, y + t_offset), sf::Color(0, 255, 255))
	};
	sf::Vertex bot[3] = {
		sf::Vertex(sf::Vector2f(x + w,      y2 + b_offset), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w + 5,  y2 + 5 + b_offset), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w + 10, y2 + b_offset), sf::Color(0, 255, 255))
	};

	sf::Vertex mid[2] = {
		sf::Vertex(sf::Vector2f(x + w + 3, y + 7), sf::Color(0, 255, 255)),
		sf::Vertex(sf::Vector2f(x + w + 7, y + 7), sf::Color(0, 255, 255))
	};

	context->draw(highlight);
	context->draw(left, 2, sf::Lines);
	context->draw(right, 2, sf::Lines);
	context->draw(mid, 2, sf::Lines);
	context->draw(top, 3, sf::PrimitiveType::LinesStrip);
	context->draw(bot, 3, sf::PrimitiveType::LinesStrip);
	context->draw(text);

}

int LNumberBox::getNumber()
{
	return num;
}

void LNumberBox::setNumber(int num)
{
	this->num = num;
}

int LNumberBox::getMax()
{
	return max;
}

int LNumberBox::getMin()
{
	return min;
}
