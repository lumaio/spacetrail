#pragma once

#include <iostream>
#include <stack>
#include <cmath>
#include "GameState.h"

typedef thor::ActionMap<std::string> EventMap;

class GameState;

class Engine
{
private:

	sf::RenderWindow* m_window;
	EventMap m_emap;

	int m_width, m_height;

	sf::Texture bg_t;
	sf::Texture stars_t;
	sf::Sprite bg_s;
	sf::Sprite stars_s;

	sf::Shader* m_globalshader;

	float time;

	std::stack<GameState*> m_states;

public:
	Engine(int=1280, int=720, std::string="Game Title");
	~Engine()=default;

	void start();
	void setState(GameState*);
	void quit() const;

	void setShader(sf::Shader*, std::string);

	EventMap& getEventMap();
	sf::RenderWindow* getContext() const;
};

