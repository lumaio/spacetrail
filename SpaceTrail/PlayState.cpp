#include "PlayState.h"
#include "LEventBox.h"

#define d() printf("FUCKING WHY %d\n", __LINE__);

PlayState::PlayState(Engine* e) :
	cleared(false),
	clicked(false),
	timer(0),
	GameState(e)
{
}

PlayState::~PlayState()
{

}

void PlayState::createEventBox(std::string text)
{
	if (!cleared) return;

	auto eb = std::make_shared<LEventBox>(1280 / 2 - 100, 720 / 2 - 50, text, text, sf::Color(0, 0, 0, 155));
	eb->setSize(200, 100);
	eb->addOption("Close", [eb, this]() { pgui->removeWidget(eb); cleared = true; });

	pgui->addWidget(eb);
	cleared = false;
}

void PlayState::init(EventMap& emap)
{
	pgui = std::make_shared<LGUI>();

	cleared = true;
	clicked = false;

	// add bounds first, otherwise images obscure buttons/other
	std::shared_ptr<LEventBox> ship_box(new LEventBox(10, 10, "Ship", ""));
	ship_box->setSize(1260, 700);

	std::shared_ptr<LEventBox> res_box(new LEventBox(20, 30, "Resources", "", sf::Color(0, 0, 0, 155)));
	res_box->setSize(200, 150);

	std::shared_ptr<LEventBox> event_box(new LEventBox(1280 - 220, 30, "Event Queue", "", sf::Color(0, 0, 0, 155)));
	event_box->setSize(200, 500);

	std::shared_ptr<LEventBox> dice_box(new LEventBox(20, 190, "Dice", "", sf::Color(0, 0, 0, 155)));
	dice_box->setSize(74 + 64 + 5, 104);

	pgui->addWidget(ship_box);
	pgui->addWidget(res_box);
	pgui->addWidget(event_box);
	pgui->addWidget(dice_box);

	// other

	printf("playstate_init\n");

	food = std::make_shared<LLabel>("Food: " + std::to_string(ship->getFood()) + "/" + std::to_string(ship->getMaxFood()), 30, 50, A_LEFT, 16, 150);
	fuel = std::make_shared<LLabel>("Fuel: " + std::to_string(ship->getFuel()) + "/" + std::to_string(ship->getMaxFuel()), 30, 75, A_LEFT, 16, 150);
	credits = std::make_shared<LLabel>("Credits: " + std::to_string(ship->credits), 30, 100, A_LEFT, 16, 150);
	

	pgui->addWidget(food);
	pgui->addWidget(fuel);
	pgui->addWidget(credits);

	one = std::make_shared<Die>(25, 200);
	two = std::make_shared<Die>(25 + 64 + 5, 200);

	roll = std::make_shared<LButton>("Roll", 25, 190+94, false, 74+64-5);
	roll->connect([this]() {
		clicked = true;
		roll->setEnabled(false);

		one->roll();
		two->roll();
	});

	pgui->addWidget(roll);

	advance = std::make_shared<LButton>("Advance", 20, 690, false, 1240);
	advance->connect([this]()
	{
		if (temp.empty()) {
			pgui->disableAll();
			turning = true;
		}
	});
	pgui->addWidget(advance);

	timer = 0;

}

void PlayState::leave()
{

}

void PlayState::update(float dt, sf::Event&)
{
	timer += dt;

	one->update(dt);
	two->update(dt);

	if (turning)
	{
		
	}

	if (one->done() && two->done() && clicked)
	{
		clicked = false;
		roll->setEnabled(true);
	}

	if (pgui != nullptr)
	{
		pgui->handleEvents(m_engine->getEventMap());
		pgui->update();
	}

}

void PlayState::render(sf::RenderWindow* context)
{
	if (pgui != nullptr)
		pgui->render(context);

	one->render(context);
	two->render(context);

}
