#include "Engine.h"

Engine::Engine(int width, int height, std::string title) :
	m_width(width),
	m_height(height),
	time(0)
{
	m_window = new sf::RenderWindow(sf::VideoMode(width, height), title, sf::Style::Close);
	m_window->setFramerateLimit(60);

	m_emap["close"] = thor::Action(sf::Event::Closed); // default clost event;
	// other default events
	m_emap["click_left"] = thor::Action(sf::Mouse::Left, thor::Action::PressOnce);
	m_emap["click_right"] = thor::Action(sf::Mouse::Right, thor::Action::PressOnce);
	m_emap["hold_left"] = thor::Action(sf::Mouse::Left, thor::Action::Hold);
	m_emap["hold_right"] = thor::Action(sf::Mouse::Right, thor::Action::Hold);

	bg_t.loadFromFile("data/images/background.png");
	stars_t.loadFromFile("data/images/stars.png");

	bg_s.setTexture(bg_t);
	bg_s.setPosition(-1280 / 2 - 4, -720 / 2 - 4);

	stars_s.setTexture(stars_t);
	stars_s.setPosition(-1280 / 2, -720 / 2);

}

void Engine::start()
{
	sf::Event event;
	sf::Clock deltaClock;

	while (m_window->isOpen())
	{
		auto dt = deltaClock.restart().asSeconds();

		m_emap.clearEvents();
		while (m_window->pollEvent(event))
			m_emap.pushEvent(event);

		if (m_emap.isActive("close"))
			m_window->close();

		if (!m_states.empty())
			m_states.top()->update(dt, event);

		m_window->clear();

		sf::Shader::bind(m_globalshader);

		time += dt;
		float time2 = time / 1.1;
		float offsetx = (bg_t.getSize().x - m_window->getSize().x);
		float offsety = (bg_t.getSize().y - m_window->getSize().y);
		bg_s.setPosition(   -offsetx + offsetx / 2 + sin(0.10 * time)*offsetx / 2, -offsety + offsety / 2 + cos(0.23 * time) * offsety / 2);
		stars_s.setPosition(-offsetx + offsetx / 2 + sin(0.10 * (time2))*offsetx / 2, -offsety + offsety / 2 + cos(0.23 * (time2)) * offsety / 2);

		m_window->draw(bg_s);
		m_window->draw(stars_s);

		if (!m_states.empty())
			m_states.top()->render(m_window);

		sf::Shader::bind(nullptr);

		m_window->display();

		event = sf::Event();
	}
}

void Engine::setState(GameState* state)
{
	printf("new state: ");
	if (!m_states.empty())
	{
		printf(" !empty! ");
		m_states.top()->leave();
		m_states.pop();
	}
	printf("clear.\n");
	m_states.push(state);

	if (!state->initialized)
		state->init(m_emap);
}

void Engine::quit() const
{
	m_window->close();
}

void Engine::setShader(sf::Shader* shader, std::string sampler)
{
	m_globalshader = shader;
	if (m_globalshader != nullptr)
		m_globalshader->setParameter(sampler, sf::Shader::CurrentTexture);
}

EventMap& Engine::getEventMap()
{
	return m_emap;
}

sf::RenderWindow* Engine::getContext() const
{
	return m_window;
}
