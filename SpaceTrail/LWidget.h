#pragma once

#include <SFML/Graphics.hpp>
#include <Thor/Input.hpp>

class LWidget
{
protected:
	float x, y, z;
	std::string _text;
	bool m_destroy;

public:

	LWidget();
	virtual ~LWidget();

	virtual void handleEvents(thor::ActionMap<std::string>&);
	virtual void update();
	virtual void render(sf::RenderWindow*);

	virtual void setText(std::string);
	virtual void setPosition(float, float) final;
	virtual bool needsDestroy() final;
	virtual void destroy();
	virtual void setEnabled(bool);

};
