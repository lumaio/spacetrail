#pragma once
#include "GameState.h"
#include "MenuState.h"
#include "LEventBox.h"

class SplashState :
	public GameState
{
private:

	std::shared_ptr<LEventBox> splashbox;
	float time;
	float time2;
	bool fadeout;
	float alpha;
	sf::RectangleShape overlay;

public:

	SplashState() = default;
	explicit SplashState(Engine*);
	~SplashState();

	void init(EventMap&) override;
	void leave() override;
	void update(float, sf::Event&) override;
	void render(sf::RenderWindow*) override;

};

