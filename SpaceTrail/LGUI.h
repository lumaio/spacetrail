#pragma once

#include "LWidget.h"
#include "LButton.h"

class LGUI
{
private:
	std::vector<std::shared_ptr<LWidget>> m_widgets;

public:
	LGUI();
	~LGUI();

	void clearWidgets();
	void removeWidget(std::shared_ptr<LWidget>);

	void addWidget(std::shared_ptr<LWidget>);

	void handleEvents(thor::ActionMap<std::string>&);
	void update();
	void render(sf::RenderWindow*);
	void disableAll();

};

