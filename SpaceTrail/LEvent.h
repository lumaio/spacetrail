#pragma once

#define lots 
#define of double

#include <iostream>
#include <string>
#include <map>


enum EVENT_TYPE {
	//The Good
	E_CUREDYSENTARY,
	E_SPEEDINCREASE,
	E_NEWCREW,

	// The Bad
	E_BROKENARM,
	E_BROKENLEG,
	E_INTERNALBLEEDING,
	E_DYSENTARY,
	E_HEADACHE,
	E_CRACKEDSKULL,
	E_ASTEROID,
	E_FOODBAG,
	E_NOTHING
};

const std::map<EVENT_TYPE, double> EVENTS = {
	// The Good
	{ E_CUREDYSENTARY,    0.01 },
	{ E_SPEEDINCREASE,    0.01 },
	{ E_NEWCREW,          0.01 },

	// The Bad
	{ E_BROKENARM,        0.05 },
	{ E_BROKENLEG,        0.05 },
	{ E_INTERNALBLEEDING, 0.05 },
	{ E_DYSENTARY,        0.05 },
	{ E_HEADACHE,         0.05 },
	{ E_CRACKEDSKULL,     0.05 },
	{ E_ASTEROID,         0.05 },
	{ E_FOODBAG,          0.05 },
	{ E_NOTHING,          0.95 }
};

struct LEvent
{
	LEvent() { }

	virtual void run() {
		printf("Blank Event\n");
	}

	static std::string getNewEvent();

};