#pragma once

#include "LWidget.h"

class LButton :
	public LWidget
{
private:

	std::function<void()> callback;
	sf::Vector2i mpos;

	bool inBounds(sf::Vector2i);

	sf::Font font;
	sf::Text text;

	float w;
	float scale;
	int c1;
	bool aw;

	bool enabled;

public:
	LButton() = default;
	LButton(std::string, float, float, bool=false, float = 150, int = 16, std::string = "data/fonts/proggy.ttf");
	~LButton();

	void connect(std::function<void()>);
	void handleEvents(thor::ActionMap<std::string>&) override;
	void update() override;
	void render(sf::RenderWindow*) override;

	void setEnabled(bool);
	bool isEnabled();

	float getWidth();

};

