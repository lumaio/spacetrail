#pragma once
#include <climits>
#include "LWidget.h"

class LNumberBox :
	public LWidget
{
private:

	enum bounds {
		B_UPPER,
		B_LOWER,
		B_NONE
	};

	sf::RectangleShape highlight;
	int num, w;
	int max, min;

	sf::Font font;
	sf::Text text;

	int t_offset;
	int b_offset;

	sf::Vector2i mpos;

	bounds inBounds();

public:
	LNumberBox(float, float, int, int = INT_MAX, int = INT_MIN, int = 0);
	~LNumberBox();

	void handleEvents(thor::ActionMap<std::string>&) override;
	void update() override;
	void render(sf::RenderWindow*) override;

	int getNumber();
	void setNumber(int);

	int getMax();
	int getMin();

};

