#include "LOutput.h"

LOutput::LOutput(float x, float y, float w, float h, bool drawbox) :
	w(w),
	h(h),
	drawbox(drawbox),
	LWidget()
{
	this->x = x;
	this->y = y;

	box.setSize(sf::Vector2f(w, h));
	box.setPosition(x, y);
	box.setFillColor(sf::Color::Transparent);
	box.setOutlineColor(sf::Color::Green);
	box.setOutlineThickness(1);

	font.loadFromFile("data/fonts/proggy.ttf");
	text.setFont(font);
	text.setCharacterSize(16);
	text.setPosition(0, 0);

}

LOutput::~LOutput()
{
}

void LOutput::print(std::string text, SEVERITY sev)
{
	auto out = std::make_shared<Output>();
	out->text = text;
	out->severity = sev;

	switch (sev)
	{
	case S_ERROR:
		out->prefix = "[ERROR] ";
		break;
	case S_INFO:
		out->prefix = "[INFO] ";
		break;
	case S_WARNING:
		out->prefix = "[WARN] ";
		break;
	case S_NONE:
		out->prefix = "";
		break;
	default:
		out->prefix = "";
		break;
	}

	output.push_back(out);

}

void LOutput::update()
{ }

void LOutput::render(sf::RenderWindow* context)
{
	int i = 0;
	for (auto& o : output)
	{
		text.setPosition(x + 5, y + i * 16);
		text.setString(o->prefix);

		switch (o->severity)
		{
		case S_ERROR:
			text.setColor(sf::Color::Red);
			text.setString(o->prefix);
			context->draw(text);
			text.setPosition(text.getPosition().x + text.getLocalBounds().width, text.getPosition().y);
			text.setColor(sf::Color::White);
			text.setString(o->text);
			context->draw(text);
			break;
		case S_INFO:
			text.setColor(sf::Color::Cyan);
			text.setString(o->prefix);
			context->draw(text);
			text.setPosition(text.getPosition().x + text.getLocalBounds().width, text.getPosition().y);
			text.setColor(sf::Color::White);
			text.setString(o->text);
			context->draw(text);
			break;
		case S_WARNING:
			text.setColor(sf::Color::Yellow);
			text.setString(o->prefix);
			context->draw(text);
			text.setPosition(text.getPosition().x + text.getLocalBounds().width, text.getPosition().y);
			text.setColor(sf::Color::White);
			text.setString(o->text);
			context->draw(text);
			break;
		case S_NONE:
			text.setPosition(x + 5, y + 19 + i * 16);
			text.setColor(sf::Color::White);
			text.setString(o->text);
			context->draw(text);
			break;
		}

		if (text.getPosition().y + text.getLocalBounds().height > x + h)
			output.erase(output.begin());

		i++;
	}

	if (drawbox)
		context->draw(box);
}
