#pragma once

#include <stack>
#include "GameState.h"
#include "LGUI.h"
#include "LButton.h"
#include "LLabel.h"
#include "Ship.h"
#include "LEvent.h"
#include "Die.h"

class PlayState :
	public GameState
{
private:
	std::shared_ptr<LGUI> pgui;

	std::stack<std::shared_ptr<LEvent>> eventstack;

	std::vector<std::shared_ptr<LButton>> temp;

	bool cleared;
	bool clicked;

	bool turning;

	std::shared_ptr<LLabel> food;
	std::shared_ptr<LLabel> fuel;
	std::shared_ptr<LLabel> credits;

	std::shared_ptr<LButton> roll;

	std::shared_ptr<Die> one;
	std::shared_ptr<Die> two;

	std::shared_ptr<LLabel> lol;

	std::shared_ptr<LButton> advance;

	float timer;

	void createEventBox(std::string);

public:
	PlayState()=default;
	explicit PlayState(Engine*);
	~PlayState();

	void init(EventMap&) override;
	void leave() override;
	void update(float, sf::Event&) override;
	void render(sf::RenderWindow*) override;

};

extern GameState* playstate;
