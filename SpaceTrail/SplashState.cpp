#include "SplashState.h"

SplashState::SplashState(Engine* e) :
	GameState(e)
{
}

SplashState::~SplashState()
{
}

void SplashState::init(EventMap& emap)
{
	splashbox = std::make_shared<LEventBox>(1280 / 2 - 85, 720 / 2 - 25, "Space Trail", "Developed by: (Lumaio)");
	splashbox->setSize(85*2, 50);
	
	splashbox->addOption("Skip", [&]() {m_engine->setState(menustate); });

	time = 0;
	time2 = 0;
	fadeout = false;
	alpha = 0;

	overlay.setSize(sf::Vector2f(1280, 720));
	overlay.setFillColor(sf::Color(0, 0, 0, alpha));
}

void SplashState::leave() { }

void SplashState::update(float dt, sf::Event& e)
{
	time += dt;

	if (time > 5 && !fadeout)
	{
		time = 0;
		fadeout = true;
	}

	if (fadeout == true)
	{
		if (alpha < 255)
			alpha += dt * 75;
		else
			alpha = 255;

		overlay.setFillColor(sf::Color(0, 0, 0, alpha));
	}

	if (alpha == 255)
		time2 += dt;

	if (time2 > 1)
		m_engine->setState(menustate);

	splashbox->handleEvents(m_engine->getEventMap());
	splashbox->update();
}

void SplashState::render(sf::RenderWindow* context)
{
	splashbox->render(context);
	context->draw(overlay);
}
