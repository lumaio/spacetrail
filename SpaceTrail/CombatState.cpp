#include "CombatState.h"
#include "LLabel.h"

CombatState::CombatState(Engine* e) :
	clicked(false),
	num(1),
	time(0),
	buffer(),
	player_stamina_(0),
	verts(sf::LinesStrip),
	GameState(e)
{
}

CombatState::~CombatState()
{
}

void CombatState::init(EventMap& emap)
{
	buffer.create(1280, 720, false);

	gui = std::make_shared<LGUI>();

	auto player_box = std::make_shared<LEventBox>(200, 100, "Player Info", "");
	player_box->setSize((1280/2 - 10) - 200, 200);

	auto enemy_box = std::make_shared<LEventBox>(1280 / 2 + 10, 100, "Enemy Info", "");
	enemy_box->setSize((1280 / 2 - 10) - 200, 200);


	gui->addWidget(player_box);
	gui->addWidget(enemy_box);

	player_die_ = std::make_shared<Die>(1280/2-84, 110);
	player_crew_ = std::make_shared<Crew>();

	gui->addWidget(std::make_shared<LLabel>(player_crew_->name_first + " " + player_crew_->name_last, 210, 120, A_RIGHT, 16, 150));
	gui->addWidget(std::make_shared<LLabel>(std::to_string(int(player_crew_->health)), 210, 140, A_RIGHT, 16, 150));
	gui->addWidget(std::make_shared<LLabel>(std::to_string(player_stamina_), 210, 160, A_RIGHT, 16, 150));

	glow.loadFromFile("data/shaders/blur.vertex", "data/shaders/blur.frag");

	m_engine->setShader(nullptr, "u_texture0");

}

void CombatState::leave() { }

void CombatState::update(float dt, sf::Event& event)
{

	time += dt;
	glow.setParameter("time", time);

	if (event.type == sf::Event::MouseWheelScrolled)
	{
		if (event.mouseWheelScroll.delta < 0 && num > 1)
			num--;
		if (event.mouseWheelScroll.delta > 0 && num < 10)
			num++;
	}

	gui->handleEvents(m_engine->getEventMap());
	gui->update();

	player_die_->update(dt);
}

void CombatState::render(sf::RenderWindow* context)
{

	// init vertices
	//if (time > 0.1)
	//{
		verts.clear();
		verts.append(sf::Vertex(sf::Vector2f(1280 / 2, 0)));
		for (auto i = 1; i < 720 / 15; i++)
			verts.append(sf::Vertex(sf::Vector2f(1280 / 2 + (-((10 * cos(i)) / 2) + rand() % 10 * cos(i)), i * 15), sf::Color(rand() % 255, rand() % 255, rand() % 255)));
		verts.append(sf::Vertex(sf::Vector2f(1280 / 2, 720)));
		//time = 0;
	//}

	// draw gui elements
	gui->render(context);
	player_die_->render(context);

	// draw weird line thing
	context->draw(verts);
}

