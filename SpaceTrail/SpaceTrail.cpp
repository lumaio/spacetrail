// SpaceTrail.cpp : Defines the entry point for the console application.
//
#pragma warning(Level: Warning)

#include "Engine.h"
#include "NameGenerator.h"
#include "MenuState.h"
#include "HireState.h"
#include "PlayState.h"
#include "SplashState.h"
#include "CombatState.h"
#include "Ship.h"

GameState* menustate;
GameState* hirestate;
GameState* buystate;
GameState* playstate;
GameState* combatstate;
Ship* ship;

int main()
{
	srand(unsigned int(time(nullptr)));

	// init player ship
	ship = new Ship();
	
	auto engine = new Engine(1280, 720, "Space Trail");

	// initialize states
	hirestate = new HireState(engine);
	menustate = new MenuState(engine);
	playstate = new PlayState(engine);
	combatstate = new CombatState(engine);

	engine->setState(new SplashState(engine));
	engine->start();

	return 0;
		
}
