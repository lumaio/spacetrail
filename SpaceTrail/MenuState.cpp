#include "MenuState.h"

MenuState::MenuState(Engine* e) :
	GameState(e)
{
}

MenuState::~MenuState()
{
}

void MenuState::init(EventMap& emap)
{

	start = std::make_shared<LButton>("Start", 1280/2-75, 100);
	options = std::make_shared<LButton>("Options", 1280/2-75, 150);
	quit = std::make_shared<LButton>("Quit", 1280/2-75, 200);

	start->connect([&]() { m_engine->setState(playstate); });
	quit->connect([&]() { m_engine->quit(); });

	std::shared_ptr <LLabel> title(new LLabel("Space Trail", 1280 / 2 - 300 / 2, 50, A_CENTER, 16 * 2, 300));

	gui = std::make_shared<LGUI>();
	gui->addWidget(start);
	gui->addWidget(options);
	gui->addWidget(quit);
	gui->addWidget(title);

	std::shared_ptr<LEventBox> event(new LEventBox(100, 450, "Space Dysentery", "Because fuck you."));
	event->addOption("Fuck You", [&]() { printf("Fuck You\n"); });
	event->addOption("Close", [event, this]() { printf("And You\n"); event->destroy(); });

	gui->addWidget(event);

}

void MenuState::leave()
{ }

void MenuState::update(float dt, sf::Event& event)
{
	if (gui == nullptr) return;

	gui->handleEvents(m_engine->getEventMap());
	gui->update();
}

void MenuState::render(sf::RenderWindow* context)
{
	if (gui == nullptr) return;
	gui->render(context);
}
