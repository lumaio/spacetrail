#pragma once

#include <iostream>
#include <vector>
#include "Injury.h"
#include "NameGenerator.h"

enum ACTION
{
	A_SCAVANGE,
	A_EAT,
	A_NOTHING
};

struct Crew
{
	std::vector<Injury*> injuries;

	std::string name_first;
	std::string name_last;
	double health, dexterity, hunger, thirst;

	ACTION action;

	void calculate()
	{
		for each(Injury* i in injuries)
		{
			health    -= i->health;
			dexterity -= i->dexterity;
			hunger    -= i->hunger;
			thirst    -= i->thirst;
		}
	}

	Crew()
	{
		action = A_NOTHING;

		name_first = NameGen::NameGen();
		name_last = NameGen::NameGen();
		dexterity = rand() % 5 + 1;
		health = 10 - (dexterity + rand() % 3);
		hunger = 5;
		thirst = 5;
	}

	void setAction(ACTION a)
	{
		action = a;
	}

};
